'use-strict';
const { URL } = (function () { try { return require('url') } catch (exception) { return { 'URL': function (href) { let el = document.createElement('a'); el.href = href; return el } } } })()
const { inspect } = (function () { try { return require('util') } catch (exception) { return {} } })()
const customInspect = Symbol.for('nodejs.util.inspect.custom')
class Markdown {

    'constructor' (raw, modifiers) {
        this.content_raw = raw
        this.content_html = undefined
        // OWASP recommends using a whitelist, this is what that's for.
        // "Do NOT simply escape the list of example characters provided in the various rules. It is NOT sufficient to escape only that list.
        // Blacklist approaches are quite fragile."
        this.safe_characters = /[a-z0-9-_\'\:\/=+@,.\~\`?!£$%^*\(\)\[\{\]\}\n\ ]/i

        this.joinWith = '\n'
        this.codeblocks = 'pre-code'
        this.links = 'strict-https' // Secure-by-default (hopefully)
        this.codeblockClasscodes = {}
        this.codeblockStyle = {}
        this.codeblockPreStyle = {}

        this.JSDOM = null
        this.highlight = null

        for (const name in modifiers)
            if (this[name] !== undefined)
                this[name] = modifiers[name]
            else
                throw new ReferenceError('No such key. ' + name)
    }

    'renderInline' (raw, noLinks=false) {
        const spl = raw.split('')
        let line = ''
        let i = 0
        let status = 'text'
        let shared = {}

        for (const i in spl) {
            const char = spl[i]
            const escaped = spl[ (parseInt(i)-1).toString() ] == '\\'
            if (char == '\\' && !escaped)
                continue

            switch (status) {
                case 'link_open':
                    if (!escaped && char == ']')
                        status = 'link_closed'
                    else
                        shared.title += char
                    break

                case 'link_ref':
                    //console.log('char', char)
                    if (!escaped && char == ')') {
                        if (this.links instanceof Function)
                            line += this.links(shared.title, shared.href, this)
                        else
                            // To make sure links are safe and secure, Markdown provides several pre-crafted filters for most scenarios. Although you can always
                            // just overwrite Markdown.links with a Function and that'll also work. Feel free to add any generic filters via a PR
                            switch (this.links) {
                                case 'lax':
                                    // No checks are done on the link, although this may prevent some weird issues this also allows any URL. Beware!
                                    line += `<a href="${this.encode(shared.href)}">${this.encode(shared.title)}</a>"`
                                    break

                                case 'lax-no-js':
                                    // Checks the link to make sure it isn't a javascript executable. Anything else is allowed
                                    line += `<a href="${shared.href.startsWith('javascript:') ? '' : this.encode(shared.href)}">${this.encode(shared.title)}</a>`
                                    break

                                case 'lax-http':
                                    // A specified link may not contain any non-http protocols. Only http and https are allowed
                                    line += `<a href="${/^(http(s)?)\:/i.test(shared.href) ? this.encode(shared.href) : ''}">${this.encode(shared.title)}</a>`
                                    break

                                case 'lax-https':
                                    // A specified link may not contain any non-http protocols. Only https is allowed
                                    const href = shared.href.startsWith('javascript:') ? '' : shared.href
                                    line += `<a href="${shared.href.startsWith('https:') ? this.encode(shared.href) : ''}">${this.encode(shared.title)}</a>`
                                    break

                                case 'strict':
                                    // The url is parsed, then checked. This will cause malformed links to fail to parse
                                    line += '<a href="' + (function () { try { return new URL(shared.href) } catch (exception) { return '' } })().toString() + `">${this.encode(shared.title)}</a>`
                                    break

                                case 'strict-no-js':
                                    // The url is parsed, then checked. This will cause malformed links to fail to parse
                                    const uria = (function () { try { return new URL(shared.href) } catch (exception) { return { 'protocol': '', 'toString': function () {return ''} }}})()
                                    line += `<a href="${uria.protocol !== 'javascript:' ? uria.toString() : ''}">${this.encode(shared.title)}</a>`
                                    break

                                case 'strict-http':
                                    // The url is parsed, then checked. This will cause malformed links to fail to parse
                                    const urib = (function () { try { return new URL(shared.href) } catch (exception) { return { 'protocol': '', 'toString': function () {return ''} }}})()
                                    line += `<a href="${urib.protocol !== 'http:' || urib.protocol !== 'https:' ? '' : urib.toString()}">${this.encode(shared.title)}</a>`
                                    break

                                case 'strict-https':
                                    // The url is parsed, then checked. This will cause malformed links to fail to parse
                                    const uric = (function () { try { return new URL(shared.href) } catch (exception) { return { 'protocol': '', 'toString': function () {return ''} }}})()
                                    line += `<a href="${uric.protocol !== 'https:' ? '' : uric.toString()}">${this.encode(shared.title)}</a>`
                                    break

                                default:
                                    throw new ReferenceError('Unknown Option for Codeblocks. ' + JSON.stringify(this.codeblocks))

                            }
                        status = 'text'
                    } else
                        shared.href += char
                    break
                    

                case 'link_closed':
                    if (!escaped && char == '(')
                        status = 'link_ref'
                    else {
                        //console.log(this.links)
                        if (this.links instanceof Function)
                            line += this.links(shared.title, undefined, this)
                        else
                            line += `<a>${this.encode(shared.title)}</a>`
                        status = 'text'
                    }
                    break

                case 'italics_bold':
                    if (!escaped && char == '*')
                        if (shared.text.length == 0)
                            status = 'italics_bold'
                        else {
                            line += `<b><i>${this.renderInline(this.encode(shared.text))}</i></b>`
                            status = 'text'
                        }
                    else
                        shared.text += char
                    break

                case 'bold':
                    if (!escaped && char == '*')
                        if (shared.text.length == 0)
                            status = 'italics_bold'
                        else {
                            line += `<b>${this.renderInline(this.encode(shared.text))}</b>`
                            status = 'text'
                        }
                    else
                        shared.text += char
                    break

                case 'italics':
                    if (!escaped && char == '*')
                        if (shared.text.length == 0)
                            status = 'bold'
                        else {
                            line += `<i>${this.renderInline(this.encode(shared.text))}</i>`
                            status = 'text'
                        }
                    else
                        shared.text += char
                    break

                case 'code':
                    if (!escaped && char == '`') {
                        line += `<code>${this.encode(shared.text)}</code>`
                        status = 'text'
                    } else
                        shared.text += char
                    break

                case 'italics_alt':
                    if (!escaped && char == '_')
                        if (shared.text.length == 0)
                            status = 'underscore'
                        else {
                            line += `<i>${this.renderInline(this.encode(shared.text))}</i>`
                            status = 'text'
                        }
                    else
                        shared.text += char
                    break

                case 'underscore':
                    if (!escaped && char == '_' && share.prevTrueChar == '_') {
                        line += `<u>${this.renderInline(this.encode(shared.text))}</u>`
                        status = 'text'
                    } else
                        shared.text += char
                    if (!escaped)
                        shared.prevTrueChar = char
                    break

                case 'strikethrough':
                    if (shared.opOn !== true) {
                        if (char !== '~')
                            line += '~' + char
                        else
                            shared.opOn = true
                    } else
                        if (!escaped && char == '~' && shared.prevTrueChar == '~') {
                            line += `<strike>${this.renderInline(this.encode(shared.text))}</strike>`
                            status = 'text'
                        } else
                            shared.text += char
                        if (!escaped)
                            shared.prevTrueChar = char
                    break

                default:
                    if (escaped) {
                        line += char
                    } else {
                        switch (char) {
                            case '*':
                                shared = { 'text': '' }
                                status = 'italics'
                                continue

                            case '_':
                                shared = { 'text': '' }
                                status = 'italics_alt'
                                continue

                            case '`':
                                shared = { 'text': '' }
                                status = 'code'
                                continue

                            case '~':
                                shared = { 'text': '' }
                                status = 'strikethrough'
                                continue

                            case '\\':
                                continue

                            case '[':
                                if (!noLinks) {
                                    shared = { 'title': '', 'href': '' }
                                    status = 'link_open'
                                    continue
                                }

                            default:
                                line += char
                        }
                    }
            }
        }
        return line
    }

    'render' (raw) {
        const parsed = typeof raw == 'object' ? raw : this.parse(raw)
        let html = []
        for (const i in parsed) {
            const { content, type, language } = parsed[i]
            //console.log(content, type)
            switch (type) {
                case 'text':
                    html.push(this.renderInline(this.encode(content)))
                    break

                case 'codeblock':
                    const self = this
                    if (this.codeblocks instanceof Function)
                        html.push(this.codeblocks(language, content, this))
                    else
                        switch (this.codeblocks) {
                            case 'classify':
                                html.push(`<div class="codeblock language-${language}">${this.encode(content)}</div>`)
                                break

                            case 'pre-code':
                                html.push(`<pre class="language-${language}"><code>${this.encode(content)}</code></pre>`)
                                break

                            case 'hljs-parse':
                                if (!this.highlight)
                                    throw new ReferenceError('This option requires an optional dependency. highlight.js')
                                html.push(`<pre class="codeblock language-${language}"><code>${highlight(language, content).value}</code></pre>`)
                                break

                            case 'hljs-parse-style':
                                if (!this.highlight)
                                    throw new ReferenceError('This option requires an optional dependency. highlight.js')
                                if (!this.JSDOM)
                                    throw new ReferenceError('This option requires an optional dependency. jsdom')

                                const { 'window': { document } } = new this.JSDOM(`<pre class="codeblock language-${language}"><code>${this.highlight(language, content).value}</code></pre>`)
                                for (const colour in this.codeblockClasscodes)
                                    for (const i in this.codeblockClasscodes[colour]) {
                                        const target = this.codeblockClasscodes[colour][i]
                                        document.querySelectorAll(`.${target}`).forEach(function (elem) {
                                            elem.classList.remove(target)
                                            elem.style.color = `#${colour}`
                                        })
                                    }

                                document.querySelectorAll('pre code').forEach(function (elem) {
                                    for (const name in self.codeblockStyle)
                                        elem.style[name] = self.codeblockStyle[name]
                                    for (const name in self.codeblockPreStyle)
                                        elem.parentElement.style[name] = self.codeblockPreStyle[name]
                                })

                                html.push(document.body.innerHTML)
                                break

                            default:
                                throw new ReferenceError('Unknown Option for Codeblocks. ' + JSON.stringify(this.codeblocks))
                        }
                    break

                case 'blockquote':
                    html.push(`<blockquote>${this.renderInline(this.encode(content))}</blockquote>`)
                    break

                case 'newline':
                    html.push(`<br />`)
                    break

                case 'h_line':
                    html.push(`<hr />`)
                    break

                default:
                    if (type[0] == 'h' && type.length == 2)
                        html.push(`<${type}>${this.renderInline(this.encode(content))}</${type}>`)
                    else
                        throw new ReferenceError('Unknown Content Type. ' + JSON.stringify(type))
            }
        }
        return html.join(this.joinWith)
    }

    'parse' (raw) {
        let lines = (raw || this.content_raw).split('\n')
        let events = []
        let i = 0

        let curr = {
            'type': 'text',
            'context': []
        }
        let codeOpen = false
        for (const i in lines) {
            const line = lines[i]
            const trimmed = line.replace(/^[ ]*/, '')
            if (codeOpen) {
                if (trimmed == '```') {
                    curr.content = curr.content.join('\n')
                    events.push(JSON.parse(JSON.stringify(curr)))
                    curr.type = ''
                    curr.language = undefined
                    curr.content = []
                    codeOpen = false
                    continue
                } else
                    curr.content.push(line)
            } else {
                if (trimmed.startsWith('######'))
                    curr.type = 'h6'
                else if (trimmed.startsWith('#####'))
                    curr.type = 'h5'
                else if (trimmed.startsWith('####'))
                    curr.type = 'h4'
                else if (trimmed.startsWith('###'))
                    curr.type = 'h3'
                else if (trimmed.startsWith('##'))
                    curr.type = 'h2'
                else if (trimmed.startsWith('#'))
                    curr.type = 'h1'
                else if (trimmed.startsWith('>'))
                    curr.type = 'blockquote'
                else if (trimmed.startsWith('```')) {
                    codeOpen = true
                    const a = trimmed.slice(3)
                    curr.language = a.length && /^[a-z]*$/.test(a) ? a : 'plaintext'
                    curr.type = 'codeblock'
                    curr.content = []
                    continue
                } else
                    curr.type = 'text'

                if (line == '')
                    events.push({ 'type': 'newline' })
                else if (trimmed.startsWith('---'))
                    events.push({ 'type': 'h_line' })
                else {
                    curr.content = trimmed.replace(/^(>|[#]{1,6})( )?/, '')
                    // For anyone curious, the stringify-parse combo is used to de-reference this loops `curr` from the next one.
                    events.push(JSON.parse(JSON.stringify(curr)))
                    curr.type = ''
                    curr.language = undefined
                    curr.content = []
                }
            }
        }

        return events
    }

    'encode' (char) {
        if (typeof char !== 'string')
            throw new ReferenceError('Input must be String, not ' + typeof char)
        if (char.length == 0)
            return ''
        let a = ''
        const b = char.split('')
        for (const i in b) // Although this may not always work, It should the majority of the time. Any edgecases should be easily fixable
            a += (this.safe_characters.test(b[i])
              ? b[i]
              : `&#x${b[i].charCodeAt(0).toString(16)};`)
        //console.log(a)
        return a
    }

    'toString' () {
        if (!this.content_html)
            this.content_html = this.render()
        return this.content_html
    }

    [customInspect] (depth, opt) {
        return `${opt.stylize('{Markdown}', 'special')} -> ${inspect(this.content_raw, opt)}`
    }

}

try { module['exports'] = { Markdown } } catch (exception) {}
