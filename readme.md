# Markdown
A simple markdown parser & compiler. Implements Headers, Italics, Bold, Code, quoteblock and Codeblocks.

## About
Markdown implements a subset of the markdown specification. Some unnecessary features are not implemented at the moment
Those features include Lists, Reference Links, Image Links and "Safe" HTML.
Markdown is "secure-by-default", embedded links are ignored if they are not https, since relative links *`[somewhere](../somefile/wheredoigo.html)`* can go anywhere you will need to use `lax` link handling to be able to use them. For more information about this, see `link_ref`

## Example
For a full list of all implemented features, see [test.js](./tests/node.js).
```javascript
const { Markdown } = require('markdown')
const myPost = new Markdown('# Hey.')
console.log(myPost.toString())
```

## Overriding handlers
If you need to you can override any default assigned variables instead of having to extend the Class.  
**Do note** that Markdown is synchronous and using non-blocking code will break things  
**Also note** that you are responsible for making sure your changes can't make script tags, you can use the built-in `Markdown.encode` function to escape a String.
```js
const someMarkdown = new Markdown('Hi, [Phil Swift] Here for Flex Tape!', {
    'links': function (title, href, self) {
        return `<a>${self.encode('Something else')}</a>`
    }
})
console.log(someMarkdown.toString()) // Hi, <a>Something else</a> Here for Flex Tape!
```

### Adding JSDOM or Highlight.js
When Markdown looks for those libraries they are to be passed as follows.
```js
const post = new Markdown(content, {
    'JSDOM': require('jsdom').JSDOM,
    'highlight': require('highlight.js').highlight
})

// Or alternatively (and neater'ly)
const { JSDOM } = require('jsdom')
const { highlight } = require('highlight.js')
const post = new Markdown(content, { JSDOM, highlight })
```

### Codeblocks
You can change how codeblocks are compiled by overriding `Markdown.codeblocks`.
```javascript
const someMarkdown = new Markdown('Some nice Code\n' + '\`'.repeat(3) + '\n12345\n' + '\`'.repeat(3), {
    // 'codeblocks': 'classify', // Encodes the content then wraps it in a `div` block. <div class="codeblock language-${language}">${this.encode(content)}</div>
    // 'codeblocks': 'pre-code', // Encodes the content then wraps it in a `precode` block. <pre class="language-${language}"><code>${this.encode(content)}</code></pre>
    // 'codeblocks': 'hljs-parse', // Parses the content with Highlight.js then wraps it in a `precode` block.
    // 'codeblocks': 'hljs-parse-style', // Similar to hljs-parse except all instances of hljs classes are replaced by color codes, See the code to get an idea of what this does.
    'codeblocks': function (language, content, self) {
        console.log('Got a codeblock that\'s', language, 'with a length of', content.length)
        return `<iframe class="codeblock external-codeblock" src="/api/codeblock?id=${content}"></iframe>`
    }
})

console.log(someMarkdown.toString()) // Some nice Code\n<iframe class="codeblock external-codeblock" src="/api/codeblock?id=12345"></iframe>
```

### Links
You can also change how Links are handled by overriding `Markdown.links`.
```js
const someMarkdown = new Markdown('[View the Documentation](docs:markdown/links)', {
    // 'links' as two "modes", `lax` and `strict`. Strict will parse the URL before it is treated as safe, Lax will not.
    // `lax` mode is not recommended and is only included for compatibility purposes, use `strict` whenever possible.
    // 'links': '[mode]-no-js', // Will reject `javascript:` URI's
    // 'links': '[mode]-http', // Will only accept `http(s):` URI's
    // 'links': '[mode]-https', // Will only accept `https:` URI's
    'links': function (title, href, self) {
        // If none of these generic modes suit your usecase, you can always just override `Markdown.links` to handle them yourself.
        const rel = new URL(coolUtil.createRelativePath(href))
        return `<a href="${rel}">${self.encode(title)}</a>`
    }
})
console.log(someMarkdown.toString()) // <a href="https://docs.example.com/markdown/override#links">View the Documentation</a>
```

## "It's a class, not a String"
`Markdown ()` is a Class, not a string. Although you *can* treat it like one, but it's not.  
The submitted string is only compiled when you call `.render()` or `.toString()`, do note that calling `.toString()` will store the rendered version and
refer back to it when called.
```javascript
const someMarkdown = new Markdown('# Hello!')
console.log(someMarkdown) // {Markdown} -> '# Hello World!'
console.log(someMarkodwn.compile()) // <h1>Hello!</h1>
```

## Browser support
Although `Markdown` is made for server-side usage, I'm not stopping you from using it in a browser. Although No grantees are made that it will work  
`Markdown` is mostly String manipulation, however `Markdown.links = 'strict*'` will not work without `URL`. Any problems with missing dependencies are ignored by default
