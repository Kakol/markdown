# Contributing
If you wish to contribute feel free, although your contribution may be rejected if:
* You modify large portions of the source code without good reason.
* You change the syntax style. (Using tabs instead of spaces when the code uses spaces)
* Your code is taken from another project. (Don't go around stack-overflowing other projects onto this one, it makes the code hard to maintain)
* The code could cause problems in the future, introduces a vulnerability or is written in such a way that it is unpredictable or uncertain.
* Overly complicated. Keep things simple unless there's a good reason not to
