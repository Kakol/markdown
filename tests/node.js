const { Markdown } = require('./../')
const myPost = new Markdown(`#Header 1
## Header 2
###  Header 3
####  Header 4
##### Header 5
###### Header 6
---
[Hello World!](https://example.com/)
_Italics_
*Also Italics?*
**Bold**
***Bold Italics***
~~Strikethrough~~
\`Some code\`
${'`'.repeat(3)}javascript
const a = Math.random()
return a * 32
${'`'.repeat(3)}
>Quotes, In MY MARKDOWN?
> Also a quote

---
Notice anything off? [Flag up an issue](https://gitlab.com/Kakol/markdown/issues)`)

console.log(`${myPost}`)
