const { createReadStream } = require('fs')
const path = require('path')
console.log(`http://172.0.0.1:3000/`)
require('http').createServer(function (req, res) {
    switch (req.url) {
        case '/lib/Markdown.js':
            res.setHeader('Content-Type', 'application/javascript'); createReadStream(path.join(__dirname, './../classes/Markdown.js')).pipe(res)
            break
        default:
            res.setHeader('Content-Type', 'text/html'); createReadStream('./test.html').pipe(res)
    }
}).listen(3000, '172.0.0.1')
